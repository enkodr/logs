# LOGS

Logs is a simple logging tool that allows you to log messages to the stdout or to a file.

## Installation

For all operating systems, download a binary from the [release page](https://github.com/enkodr/logs/releases/latest).
Or try this to grab the latest official release using the command line:

```
opsys=linux  # or darwin, or windows
curl -s https://api.github.com/repos/enkodr/logs/releases/latest |\
  grep browser_download_url |\
  grep $opsys |\
  cut -d '"' -f 4 |\
  xargs curl -O -L
mv logs_*_${opsys}_amd64 logs
chmod u+x logs
```

## Usage

### Logging to stdout
```
logs "Message to log to stdout"
```

### Logging different types
```
logs --type=warning "Warning message"
logs --type=error "Error message"
logs --type=info "Info message"
```

### Logging to file
```
logs --file=/var/logs/app.log "Message to log to file and stdout"
```

### Logging only to file
```
logs --silent --file=/var/logs/app.log "Message to log only to file"
```

### Reading from stdin
```
  echo "Message to log" | logs
```