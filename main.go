package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Author = "André Sá"
	app.Name = "logs"
	app.Usage = "A simple logging tool to shout logs to stdout and/or file"
	app.UsageText = app.Name + " [global options] \"message\""
	app.Version = "0.4.1"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "file, f",
			Value: "",
			Usage: "File to append logs to",
		},
		cli.StringFlag{
			Name:  "type, t",
			Value: "info",
			Usage: "Type of information (info, error, warning)",
		},
		cli.BoolFlag{
			Name:  "silent, s",
			Usage: "No stdout output",
		},
	}

	app.Action = func(c *cli.Context) error {
		msg := ""
		if c.NArg() > 0 {
			msg = strings.Join(c.Args(), " ")
		}

		if msg == "" {
			fi, err := os.Stdin.Stat()
			if err != nil {
				panic(err)
			}
			if fi.Mode()&os.ModeNamedPipe != 0 {
				scanner := bufio.NewScanner(os.Stdin)
				for scanner.Scan() {
					msg += scanner.Text()
				}
			}
		}

		now := time.Now().Format("2006-01-02 15:04:05")
		if c.String("type") == "info" {
			msg = "[" + now + "] INFO.....: \"" + msg + "\"\n"
		} else if c.String("type") == "warning" {
			msg = "[" + now + "] WARNING..: \"" + msg + "\"\n"
		} else {
			msg = "[" + now + "] ERROR....: \"" + msg + "\"\n"
		}
		if c.Bool("silent") != true {
			fmt.Print(msg)
		}
		filename := c.String("file")
		if filename != "" {
			f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			defer f.Close()

			if err != nil {
				return err
			}

			if _, err = f.WriteString(msg); err != nil {
				return err
			}
		}
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
